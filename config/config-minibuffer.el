(provide 'config-minibuffer)

(fido-vertical-mode)
(setq completion-auto-select t)

(require 'compat)
(require 'marginalia)
(marginalia-mode)

(require 'embark)
