(provide 'config-evil)

(setq evil-want-integration t
      evil-want-keybinding nil)
(require 'evil)
(evil-mode)

(require 'annalist)
(require 'evil-collection)
(evil-collection-init)
