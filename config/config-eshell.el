(provide 'config-eshell)

(setq eshell-hist-ignoredups t
      eshell-scroll-to-bottom-on-input t)

(require 'eshell-git-prompt)
(eshell-git-prompt-use-theme 'powerline)
