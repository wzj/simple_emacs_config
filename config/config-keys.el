(provide 'config-keys)

(require 'general)
(general-evil-setup)
;; my leader
(general-create-definer wzj/leader
      :keymaps '(normal insert visual emacs)
      :prefix "SPC"
      :global-prefix "C-SPC")

;; toggle electric pair
(wzj/leader "tp" 'electric-pair-mode)
;; eval group
(wzj/leader "eb" 'eval-buffer)
;; dired mode
(general-nmap :keymaps 'dired-mode-map	
  "h" 'dired-up-directory
  "l" 'dired-view-file
  "j" 'peep-dired-next-file
  "k" 'peep-dired-prev-file)
;; find group
(wzj/leader
  "ff" 'find-file
  "fr" 'recentf)
;; embark group
(wzj/leader
  "C-." 'embark-act
  "C-;" 'embark-dwin
  "C-h B" 'embark-bindings)
