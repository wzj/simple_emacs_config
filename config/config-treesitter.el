(provide 'config-treesitter)

(setq treesit-language-source-alist
  '((bash . ("git@github.com:tree-sitter/tree-sitter-bash.git"))
    (python . ("git@github.com:tree-sitter/tree-sitter-python.git"))))

(setq major-mode-remap-alist
  '((python-mode . python-ts-mode)
    (sh-mode . bash-ts-mode)))

