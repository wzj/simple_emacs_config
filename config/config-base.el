(provide 'config-base)

;; scroll
(setq scroll-step 1)

;; face attribute
(set-face-attribute 'default nil :font "FiraCode Nerd Font" :height 150 :weight 'medium)
(set-face-attribute 'fixed-pitch nil :font "Droid Sans Mono Nerd Font" :height 145 :weight 'medium)

;; line and column number
(column-number-mode)
(display-line-numbers-mode)
(setq display-line-numbers-type 'relative)

;; prettify symbols
(global-prettify-symbols-mode)

;; y-or-n globally
(defalias 'yes-or-no-p 'y-or-n-p)

;; back to where i was
(save-place-mode)

;; save file history
(recentf-mode)
(add-to-list 'recentf-exclude "^/\\(?:ssh\\|su\\|sudo\\)?x?:")
(setq history-length 25)

;; transparency
(set-frame-parameter nil 'alpha-background 80) ; For current frame
(add-to-list 'default-frame-alist '(alpha-background . 80))

;; proxy
(setq url-proxy-services '(("https" . "127.0.0.1:7891")
			   ("http" . "127.0.0.1:7891")
			   ("socks5" . "127.0.0.1:7890")))
