(provide 'config-init)

(require 'config-base) ;; all default options not managed in a third party plugin
(require 'config-evil) ;; vim emulation
(require 'config-minibuffer) ;; how minibuffer looks
(require 'config-modeline) ;; doom-modeline
(require 'config-dashboard)
(require 'config-treesitter) ;; manage code highlight with treesitter
(require 'config-lang) ;; manage code highlight unprovided by treesitter
(require 'config-org) ;; manage org settings
(require 'config-eshell) ;; manage eshell settings
(require 'config-lsp) ;; use lsp-bridge
(require 'config-latex) ;; how i write articles
(require 'config-keys) ;; use general.el to manage all key bindings
