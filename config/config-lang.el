(provide 'config-lang)

;; nix lang
(require 'reformatter)
(add-to-list 'load-path "~/.emacs.d/lib/nix-mode/")
(with-eval-after-load 'info
  (info-initialize)
  (add-to-list 'Info-directory-list
               "~/.emacs.d/site-lisp/nix-mode/"))
(require 'nix-mode)
(add-to-list 'auto-mode-alist '("\\.nix\\'" . nix-mode))
(add-to-list 'auto-mode-alist '("\\.drv\\'" . nix-drv-mode))
